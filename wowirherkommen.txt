<!doctype html>
<html lang="de">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
        <meta charset="utf-8">
        <title>wowirherkommen</title>
		<style>
		ul {
		list-style-type: none;
		margin: 0;
		padding: 0;
		}

		li {
		display: inline;
		}		
		.body { 
			background-color: #b4d2ff;
			}
		.ueberschrift {
			color: lavenderblush;
			font-family: "Times New Roman", Times, serif;
		border: 5px outset blue;
		text-align: center;
		font-size: 15px;
		}
		.rotebio {
		color: tomato;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		}
		.gruenebio {
		color: forestgreen;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		}
		.weissebio {
		color: ivory;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		}
		.andere {
		color: lightslategrey;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		font-style: italic;
		}
    </head>
    <body>
        <!--diese Seite bitte nicht verändern!-->
        <header>
            <h2>Wo wir herkommen</h2>
        </header>
        <nav>
            <ul>
                <li><a href="index.html">Start</li>
                <li><a href="waswirtun.html">Was wir tun</a></li>           <!-- dev_was Solms-->
                <li><a href="werwirsind.html">Wer wir sind</a></li>         <!-- dev_wer Grämer-->
                <li>Wo wir herkommen</li>                                   <!-- dev_wo Gina Puthenpurackal-->
                <li><a href="news.html">News</a></li>                       <!-- dev_news Wörle-->
                <li><a href="produkte.html">Produkte</a></li>               <!-- dev_produkte Katzke-->
                <li><a href="presse.html">Presse</a></li>                   <!-- dev_presse Preckel-->
                <li><a href="gast.html">Gästebuch</a></li>                  <!-- dev_gast Gümüs-->
                <li><a href="forms.html">Formulare</a></li>                 <!-- dev_forms Nasseri-->
                <li><a href="upload.html">Dateien hochladen</a></li>        <!-- dev_upload Verhülsdonk-->
                <li><a href="rss.html">Rss</a></li>                         <!-- dev_rss Jawad-->
                <li><a href="anfahrt.html">Anfahrt</a></li>                 <!-- dev_anfahrt Foerster-->
                <li><a href="kontakt.html">Kontakte</a></li>                <!-- dev_kontakt Reinoß-->
                <li><a href="impressum.html">Impressum</a></li>             <!-- dev_impressum Pestsova-->
            </ul>
        </nav>
        <main>
            <h1>Der Start</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien. Nam eu neque vulputate diam rhoncus faucibus. Curabitur quis varius libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam placerat sem at mauris suscipit porta. Cras metus velit, elementum sed pellentesque a, pharetra eu eros. Etiam facilisis placerat euismod. Nam faucibus neque arcu, quis accumsan leo tincidunt varius. In vel diam enim. Sed id ultrices ligula. Maecenas at urna arcu. Sed quis nulla sapien. Nam felis mauris, tincidunt at convallis id, tempor molestie libero. Quisque viverra sollicitudin nisl sit amet hendrerit. Etiam sit amet arcu sem. Morbi eu nibh condimentum, interdum est quis, tempor nisi. Vivamus convallis erat in pharetra elementum. Phasellus metus neque, commodo vitae venenatis sed, pellentesque non purus. Pellentesque egestas convallis suscipit. Ut luctus, leo quis porta vulputate, purus purus pellentesque ex, id consequat mi nisl quis eros. Integer ornare libero quis risus fermentum consequat. Mauris pharetra odio sagittis, vulputate magna at, lobortis nulla. Proin efficitur, nisi vel finibus elementum, orci sem volutpat eros, eget ultrices velit mi.</p>
        <div class="ueberschrift"><h1>Biotechnologie</h1>
		<div class="rotebio"><h2>Die rote Biotechnologie</h2><br />...befasst sich mit den medizinischen Nutzen für und Auswirkungen auf Diagnostik und Heilmethoden von Krankheiten. Dabei handelt es sich um den größten Sektor der Branche. Durch moderne Gentechnik können Biotechnologen Zellen manipulieren und mit ihrer Hilfe zum Beispiel neue Medikamente gegen Diabetes herstellen.
		</div>
<div class="weissebio"><h2>Die weiße Biotechnologie</h2><br />Von der weißen Biotechnologie reden wir, wenn es um industrielle Zusammenhänge geht. Wer in diesem Sektor arbeitet, forscht beispielsweise an Wasch- und Reinigungsmitteln, Chemikalien oder Vitaminpräparaten. Ziel ist es dabei, Ressourcen und die Umwelt weitgehend zu schonen.
</div>
<div class="gruenebio><h2>Die grüne Biotechnologie</h2><br />...widmet sich der landwirtschaftlichen Nutzung von biologischen Prozessen. In diesem Metier geht es unter anderem darum, mithilfe der Genetik möglichst ertragreiche Ernten zu züchten. So kannst Du etwa die Größe oder den Geschmack einer Frucht manipulieren.
</div>
<div class="andere"><h2>Andere Sparten</h2>Die Bezeichnung der anderen Sparten ist teilweise noch vage. Verschiedene Institute und Organisationen verwenden die Begriffe unterschiedlich. Im Allgemeinen steht die graue Biotechnologie für die Abfallwirtschaft, etwa bei der Aufbereitung von Trinkwasser. Umweltschutz ist oft mit der braunen Biotechnologie assoziiert. Wer in der blauen Biotechnologie arbeitet, beschäftigt sich zum Beispiel mit Lebewesen wie Bakterien, die im Meer vorkommen.
</div>		
		</main>
        <footer>
            <p>Ich bin die Fußzeile vom Start</p>
        </footer>
  </body>
</html>
